from django.db import models
from django import forms
from .models import Status

class Status_Form(forms.Form):
    attrs = {
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'What is on your mind?', 
        'size'    : '100'
        }
    mystatus = forms.CharField(label = '',required = True, max_length = 300,widget = forms.TextInput(attrs=attrs))
