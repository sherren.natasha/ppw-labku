from django.apps import AppConfig


class Lab7AppsConfig(AppConfig):
    name = 'lab7_apps'
