from django.shortcuts import render
from django.shortcuts import render
from django.http import HttpResponse
from .models import Status
from .forms import Status_Form


# Create your views here.
response ={}
def index (request):
    mystatus = Status.objects.all()
    response['mystatus'] = mystatus
    response['form'] = Status_Form
    html = 'hello.html'
    form = Status_Form(request.POST or None)
    if(request.method == 'POST'):
        response['mystatus'] = request.POST['mystatus']
        save_status = Status(mystatus = response['mystatus']) 
        save_status.save()
        save_status = Status.objects.all()
        response['mystatus'] = save_status
    return render(request, html, response)

def profile(request):
    return render(request , 'profile.html', {})

