from django.urls import path
from . import views

urlpatterns = [
    path('',views.index, name = 'index'),
    path('hello.html', views.index, name='hello'),
    path('profile.html',views.profile, name='profile'),
]
