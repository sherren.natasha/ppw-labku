from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time
from django.test import *
from django.urls import resolve
from .views import index
from .models import Status

class NewVisitorTest(unittest.TestCase):
    def setUp(self):
        #menentukan browser mana yang akan dibuka
        chrome_options = Options()
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)

    def tearDown(self):
        #memerintahkan agar browser menunggu selama 3 detik sebelum keluar
        self.selenium.implicitly_wait(3)
        self.selenium.quit()
        super(NewVisitorTest,self).tearDown()

    def test_can_start_status(self):
        selenium = self.selenium
        #mendapatkan url localhost dan mengecek apa 'My Status' ada di title website
        selenium.get('http://trap-is-normal.herokuapp.com')

        #memasukkan string coba-coba ke box input
        time.sleep(5)
        status_box = selenium.find_element_by_name('mystatus')
        status_box.send_keys('Coba Coba')
        status_box.submit()
        time.sleep(5)
        #mengecek apa ada 'halo' di header website
        header_text = selenium.find_element_by_tag_name('h1').text
        self.assertIn('Halo',header_text)

        #mengecek apa ada output coba-coba setelah di submit
        self.assertIn('Coba Coba', self.selenium.page_source)

    def test_layout(self):
        selenium = self.selenium
        selenium.get('http://trap-is-normal.herokuapp.com')
        header_text2 = selenium.find_element_by_tag_name('h1').text
        self.assertIn('apa', header_text2)

    def test_layout_2(self):
        selenium = self.selenium
        selenium.get('http://trap-is-normal.herokuapp.com')
        header_text3 = selenium.find_element_by_tag_name('h1').text
        self.assertIn('kabar', header_text3)


class MyFirstTests(TestCase):
    
    def test_hello(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_hello_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_template_hello(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'hello.html')

    def test_landing_page_(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn('Halo, apa kabar?',html_response)
    def test_create_object_model (self):
        status_message = Status.objects.create(mystatus = 'ok')
        counting_object_status = Status.objects.all().count()
        self.assertEqual(counting_object_status, 1)

    def test_profile_page_(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn('My Profile',html_response)

if __name__ == '__main__':
    unittest.main(warnings='ignore')

